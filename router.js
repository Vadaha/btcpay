module.exports = function (app) {
  var config = require('./config.js');
  var BTCAddressController = require('./controllers/address')
  var BlockController = require('./controllers/block')
  var TransactionsController = require('./controllers/transactions');
  var Bitcoind = require('./controllers/bitcoind');
  var db = require('./db');  


app.post('/sendBitcoin', BTCAddressController.SendBitcoins);


app.post('/newpack', BTCAddressController.NewAddressPack );
//*************************
//  FROM BITCOIND 
// new Transaction from bitcoind
app.get('/transactions/:txid', TransactionsController.newTxFromBitcoind);

// new Block from bitcoind
app.get('/block/:txid',BlockController.newBlockBitcoind);

//*************************
//  FROM SHOP 

app.get('/invoice/:invoice_par/:secure', BTCAddressController.htmlFrame);

app.get('/payments_from/:txfrom/:secure', TransactionsController.paymentsFrom);

//*************************
// SHOP Emulator
app.post('/sendnewtx/:secure', TransactionsController.newTxInShop );

app.get('/shop_cart/:secure', function (req, res) {
  url= db.url();
  res.render('shop_cart', { title : 'index', url: url});

});
-

//*************************
// ADMIN Emulator
app.get('/admin/:secure', function (req, res) {
  Bitcoind.getbalanceall(  function (err, balance) {
    if (err) {
      console.log(err);
      res.status(500).send('Something broke!');
    } else {
      res.render('index', { title : 'index', balance : balance });
    }
    
  });
});

// Functions of ADMIN Emulator
// encryptwallet - TODO: need remove this functions before production version
app.post('/encryptwallet', function (req, res) {    Bitcoind.encryptwallet(req, res, config.passphrase ) });
// DEcryptwallet - TODO: need remove this functions before production version
app.post('/walletpassphrase', function (req, res) { Bitcoind.walletpassphrase(req, res, config.passphrase )});

app.post('/getbalanceall', function (req, res) {    Bitcoind.getbalanceallJSON( req, res ) ; });


}