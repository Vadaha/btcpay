//
var TransactionsController = require('../controllers/transactions');
var TransactionModel = require('../models/transactions');
var BlockModel = require('../models/block');
var Bitcoind = require('../controllers/bitcoind');
var db = require('../db');  




exports.newBlockBitcoind = function (req, res) {
	var txid = req.params.txid;
	console.log("NEW BLOCK - !!!!  "+ txid);

	Bitcoind.client().cmd('getblock', txid, function (err, trxinfo) {
		if (err || !txid) {
			if (err) {
				console.log(err);
			}
			return res.json({ success : false});
		} 
		else 
		{

			console.error(trxinfo);
            // update confirmation
            TransactionModel.getUnconfirmed( 	
            	function ( rows ) {
            		rows.forEach(function(entry) 
            		{
	                	// update confirmations on 
	                	Bitcoind.UpdateTXID(entry.txid, entry.transaction_id);
	                });
            	}
            );

			// send not sended transactions to shop.
			TransactionsController.sendUnsendedTx();

		}
	});
	return res.json({ success : true});

}