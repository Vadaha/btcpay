     var config = require('./config.js');
     var mysql      = require('mysql');
     var state = { db: null };

function done() {

	return true;
}
exports.connect = function(){
	if(state.db) {
		return done();
	}

	state.db = mysql.createConnection({
	   host     : 'localhost',
	   user     : 'root',
	   password : config.MYSQLPW,
	   database : 'paybtc'
	 });
	state.db.connect(function(err){
		 if(!err) {
		     console.log("Database is connected ... \n\n");  
		 } else {
		     console.log("Error connecting database ... \n\n");  
		     setTimeout(connect, 2000);
		 }
	});
	state.db.on('error', function(err) {
    console.log('db error!!', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      connect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
	return done();
}

exports.get =function(){
	this.connect();
	return state.db;
}
exports.url =function(){
	
	return "http://"+config.ADDRIP+":8081/";
}

/*
-- --------------------------
-- Bitcoin payment server  --
-- --------------------------

create database pay_btc;

use pay_btc;

-- drop table btc_adresses;
create table btc_adresses
(
address_id INT NOT NULL AUTO_INCREMENT
, address varchar(36) NOT NULL
, invoice_code INT  -- assigned to invoice. if empty - address not in use
, pack_address_id INT NOT NULL
, date_activation DATETIME  -- life-time set in settings
, PRIMARY KEY ( address_id )
);

-- drop table transactions;
create table transactions
(
transaction_id INT NOT NULL AUTO_INCREMENT
, invoice_code INT
, value NUMERIC(20,8) NOT NULL
, date DATETIME
, send_status ENUM ('W','K') -- waiting('W'),sendOK('K')
, confirmations INT
, txid varchar(65)
, unic varchar(30) UNIQUE
, PRIMARY KEY ( transaction_id )
);


-- drop table pack_adresses;
create table pack_adresses
(
pack_address_id INT NOT NULL AUTO_INCREMENT
, name varchar(8) NOT NULL -- same to bitcoind <account>
, status ENUM ('A','N','W','E','T')
-- {active('A'), next('N'), new('W'), ended('E'), trash('T')}
, PRIMARY KEY ( pack_address_id )
);
*/
