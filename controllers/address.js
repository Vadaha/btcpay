

var db = require('../db');  
var Bitcoind = require('../controllers/bitcoind');
var Secure = require('../controllers/secure');
var BTCAddressModel = require('../models/address');




exports.htmlFrame = function (req, res) {
	var invoice = req.params.invoice_par;
	var secure = req.params.secure;
        // invoice correct?
        // check is in database?
        // write if new to next address

        // +++invoice correct?


var getInfoByWallet = function(wallet){                      // get info about invoice payments
	Bitcoind.client().cmd('getaccount', wallet, function (err, acc) {
		if (err || !acc) {
			if (err) {
				console.log(err);
			}
			return res.redirect('/');
		} else {

			console.log("acc: /////" + acc + '  '+ wallet);
			Bitcoind.client().cmd('getreceivedbyaddress', wallet, function (err, balance) {
				if (err) {
					console.log(err);
					res.status(500).send('Something broke!');
				} else {
					Bitcoind.client().cmd('listtransactions', acc, function (err, transactions) {
						if (err) {
							console.log(err);
							res.status(500).send('Something broke!');
						} else {
							for (var i = 0; i < transactions.length; i++) {
								transactions[i].time = new Date(transactions[i].time * 1000);
							}
							res.render('invoice', { title : 'invoice '+invoice, wallet : wallet, balance : balance, transactions : transactions, invoice: invoice });
						}
					});
				}
			});
		}
	});}



	var addNewInvoice = function(invoice){

		db.get().query('UPDATE btc_adresses SET invoice_code = ? WHERE invoice_code = "" order by address_id limit 1', [invoice], function(err, rows) {
			if (!err) {
				console.log('Assigned new address: ', invoice);
				db.get().query('select address_id, address from btc_adresses WHERE invoice_code = ?', [invoice], function(err, rows) {
					if (!err) {
						numRows = rows.length;
						if(numRows>0){
							console.log(" we make link invoice N :", invoice);
							wallet = rows[0].address;
                            // get info about invoice payments
                            getInfoByWallet(wallet);
                        }
                    }
                });
			}
			else

				console.log(err);
		});
	}

	
        //check if invoice already in db...
        Secure.checkPair(secure,invoice, function(){
        	var wallet = BTCAddressModel.checkWalletByInvoice(invoice, function(wallet){
        		if(wallet) {
        			console.log("WALLET IN DB: " + wallet);
        			getInfoByWallet( wallet );
        		}
        		else 
        			addNewInvoice( invoice );
        	}      );
        });

    }

    function newpackname()
    {
    	var text = "";
    	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    	for( var i=0; i < 8; i++ )
    		text += possible.charAt(Math.floor(Math.random() * possible.length));

    	return text;
    }

    exports.SendBitcoins =  function (req, res) {
    	var wallet = req.body.wallet;
    	var amount = req.body.amount; 

    	amount = parseFloat(amount);

    	if (!addr || addr.length == 0) {
    		return res.json({ success : false });
    	}

    	if (isNaN(amount)) {
    		return res.json({ success : false });
    	}

    	Bitcoind.client().cmd('getaccount', wallet, function (err, acc) {
    		if (err || !acc) {
    			if (err) {
    				console.log(err);
    			}

    			return res.json({ success : false });
    		} else {
    			Bitcoind.client().cmd('getbalance', acc, function (err, balance) {
    				if (err) {
    					console.log(err);
    					return res.json({ success : false });
    				} else {
    					if (amount > balance) {
    						return res.json({ success : false });
    					} else {
    						Bitcoind.client().cmd('sendfrom', acc, addr, amount, function (err, tid) {
    							if (err) {
    								console.log(err);
    								return res.json({ success : false });
    							} else {
    								return res.json({ success : true, tid : tid });
    							}
    						});
    					}
    				}
    			});
    		}
    	});
    }
    exports.NewAddressPack =  function (req, res) {
    	var packname=newpackname();
    	BTCAddressModel.insertNewPackName( packname ,  function(err, result) {

    		if (!err) {
    			pack_id =  result.insertId;
    			console.log("pack_id: " + pack_id + " - " + packname);

    			for( var i=0; i < 100; i++ ){

    				Bitcoind.client().cmd('getnewaddress', packname, function (err, addr) {
    					if (err) {
    						console.log(err);
    						return res.json({ success : false });
    					} else {

    					BTCAddressModel.insertNewAddress( addr, pack_id ,  function(err, result) {	
    							if (!err)
    								console.log('Address No: ', result.insertId, " is ", addr);
    							else {
    								console.log(err);
    								return res.json({ success : false }); }
    						});
    					}
    				});

    			}
    			return res.json({ success : true, packid : pack_id });

    		}
    		else {
    			console.log('Error while performing Query.');
    			console.log(err);
    			return res.json({ success : false });
    		}

    	});
        /////////////

    }

