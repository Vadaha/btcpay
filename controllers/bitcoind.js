//
var bitcoin = require('bitcoin');
var db = require('../db');  
var TransactionsController = require('../controllers/transactions');
var TransactionModel = require('../models/transactions');

var state = { client: null };

function done() {
	return true;
}
exports.connect = function(){
	if(state.client) {
		return done();
	}

	state.client = new bitcoin.Client({
		host: 'localhost',
		port: 8332,
		user: 'bitcoinrpc',
		pass: '3QtnxrB7P5y4EpBdad1MkCeB2RHmArvcarw7udgXsAce'
	});
	return done();
}

exports.client =function(){
	this.connect();
	return state.client;
}
exports.UpdateTXID=function(txid,transaction_id){
	this.client().cmd('gettransaction', txid, function (err, trxinfo) {
		if (err || !trxinfo) {
			if (err) {
				console.log(err);
			}
		} else {
			console.error('!!!trxinfo:  '+ trxinfo);
			conf = trxinfo.confirmations;
			console.error("updateConfirmations + " + conf + '  ' +txid);
			TransactionModel.updateConfirmations(conf, txid, transaction_id);
		}
	});
}
exports.encryptwallet=function (req, res, passphrase) {
	this.client().cmd('encryptwallet', passphrase, function (err) {

		if (err) {
			console.log(err);
			return res.json({ success : false });
		}
		else  {
			return res.json({ success : true});
		}


	});
}
exports.walletpassphrase=function (req, res, passphrase) {
	this.client().cmd('walletpassphrase', passphrase, 20, function (err) {

		if (err) {
			console.log(err);
			return res.json({ success : false });
		}
		else  {
			return res.json({ success : true});
		}


	});
}

exports.getbalanceall=function (cb) {
	this.client().cmd('getbalance',  function (err, balance) 
	{ 
		cb(err, balance) 
	});
}
exports.getbalanceallJSON=function (req, res) {
	this.getbalanceall( function (err, balance) { 
    if (err) {
      console.log(err);
      return res.json({ success : false });
    }  else {
      return res.json({ success : true, balance : balance +2});
    }
  })
	};


