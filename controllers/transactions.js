var db = require('../db');  
var querystring = require('querystring');
var http = require('http');
var request = require('request');
var TransactionModel = require('../models/transactions');
var Bitcoind = require('../controllers/bitcoind');
var BTCAddressModel = require('../models/address');
var Secure = require('../controllers/secure');
var Config = require('../config.js');

exports.updateConfirmations = function ( conf, transaction_id) {
	TransactionModel.updateConfirmations(conf, transaction_id);
	
}
exports.paymentsFrom = function (req, res) {
	var txfrom = req.params.txfrom;
	var secure = req.params.secure;
	//todo: secure check
	Secure.checkPair(secure,txfrom, function(){
		TransactionModel.getTransactionsFrom( txfrom,	function(err, rows) {
			if (err) { 
				console.log(err); 
				res.status(500).send('Something broke!');
			}   
			res.send(rows);

		})
	});
}

// send not sended transactions to shop.
exports.sendUnsendedTx = function ( conf, transaction_id) {
	db.get().query('select transaction_id,invoice_code,value,date  from transactions WHERE confirmations >5 and send_status!="K" ',
		function(err, rows) {
			if (err) { console.log(err); } 
			else 
			{   

				var data = {};
				rows.forEach(function(entry) {


					data = 	querystring.stringify(
					{
						transaction_id:entry.transaction_id, 
						invoice_code:entry.invoice_code,
						amount:entry.value,
						date:entry.date
					});
					
					var secured = Secure.crypt(entry.transaction_id.toString());
					request.post({
						headers: {'content-type' : 'application/x-www-form-urlencoded'},
						url:     'http://' + Config.SHOP_URL +  ''+secured,
						body:    data
					}
					, function (error, response, body) {
						if (!error && response.statusCode == 200) {

							bodyy = JSON.parse(body);
							if (bodyy.success==true ) {
								Secure.checkPair ( bodyy.crypto, bodyy.transaction_id.toString(), 
									function(){
										console.log("need update transaction: " + bodyy.transaction_id);
										// bodyy.transaction_id == entry.transaction_id, 
										db.get().query("update transactions set send_status ='K' where transaction_id=?" , bodyy.transaction_id, function(err, result) {
											if (!err) {

												console.error(err);

											}
											else {
												console.error('Error while performing Query.');
												console.error(err);
												return false;
											}
										});


									});
							}
							else
							{
								console.log("Error in sending transaction: " + bodyy.transaction_id)
                                // todo update transaction what it success send to shop
                            }

                        }
                        else {
                        	console.log(error)

                        }
                    });

				});
			}
		});    
}    
exports.newTxFromBitcoind = function (req, res) {
	var txid = req.params.txid;

	var amountall,amount =0;
	var conf = 0;
	var btc ="";
	Bitcoind.client().cmd('gettransaction', txid, function (err, trxinfo) {
		if (err || !trxinfo) {
			if (err) {
				console.log(err);
			}
			return res.redirect('/');
		} else {

			console.error('NEW TX: '  + trxinfo);
			conf = trxinfo.confirmations;
			trxinfo.details.forEach(function(entry) {
				console.error("-----");
				console.log(entry);
				if(entry.category=="receive"){
					amountall = amountall + entry.amount;
					amount = entry.amount;
					address = entry.address;
					BTCAddressModel.getInvoiceByWallet(address,function(invoice_code) {
						console.error("-invoice_code- "+ invoice_code);
						if(invoice_code){
							var unic;
              		/*	var txslice = txid.slice(1, 22);
              		unic = txslice.concat(String(invoice_code)).slice(0, 29);*/
              		var dt = new Date().toISOString().slice(0, 19).replace('T', ' ');

              			unic = String(invoice_code).concat(txid).slice(0, 29); // !! it is because in one tx we can many in and many out to many address

              			var post  = {  
              				txid: txid, 
              				value: amount,
              				confirmations: conf,
              				invoice_code: invoice_code,
              				unic: unic,
              				DATE: dt
              				
              			};
              			//TransactionModel.AddTransaction()

              			var sqlstr = 'insert into transactions SET ? ON DUPLICATE KEY UPDATE confirmations="' +conf+ '"';
              			db.get().query(sqlstr, post, function(err, result) {
              				if (!err) {

              					res.render('transactions', { title : "transactions" });
              					return true;
              				}
              				else {
              					console.error('Error while performing Query.');
              					console.error(err);
              					return false;
              				}
              			});

              		}
              		else console.error('ERROR: NO INVOICE CODE ASSOCIATED WITH ADDRESS!!')

              	});

				}
			});
			
			



		}
	});


}
// shop side
exports.newTxInShop = function (req, res) { 
	var txnew = req.body;
	var secure = req.params.secure;
	console.log(txnew.transaction_id + "  txnew, ----amount: " + req.body.amount);
 //todo: change secure in return. now work in develop mode 1 secure to send and same to return.
 return res.json({ success : true, transaction_id : txnew.transaction_id, crypto: secure });
}