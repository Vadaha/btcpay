var express = require('express');
var hbs = require('hbs');
var fs = require('fs');
var config = require('./config.js');
var router = require('./router.js');
var bitcoin = require('bitcoin');
var mysql      = require('mysql');
var crypto = require('crypto');
var nodeadmin = require('nodeadmin');
var bodyParser = require('body-parser');
    
app = express();

app.set('port', config.PORT);
app.set('addr', config.ADDR);

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.use(bodyParser.json());  

app.use(express.static(__dirname + '/public'));

//app.use(express.logger());
//app.use(app.router);
app.use(nodeadmin(app));

// register partial
var partialsDir = __dirname + "/partials/";
hbs.registerPartial('head', fs.readFileSync(partialsDir + "head.hbs", 'utf-8'));
hbs.registerPartial('footer', fs.readFileSync(partialsDir + "footer.hbs", 'utf-8'));


app.listen(app.get('port'), app.get('addr'), function () {
    console.log("Server started at " + app.get("addr") + ":" + app.get("port"));
    router(app, crypto);
});