var db = require('../db');  
  
exports.checkWalletByInvoice = function ( invoice, cb) {
	   db.get().query('select address_id, address from btc_adresses WHERE invoice_code = ?', [invoice], function(err, rows) {
                if (!err) {
                	var wallet;
                    if(rows.length>0)
                    	wallet =  rows[0].address;
                    else
                    	wallet = false;

                    cb( wallet );
                   
                }
                else {
                	console.log(err);
                	return false;
                }
            });
        }
exports.getInvoiceByWallet = function ( address, cb) {
	   db.get().query('select invoice_code from btc_adresses WHERE address = ?', [address], function(err, rows) {
                if (!err) {
                	var invoice_code;
                    if(rows.length>0)
                    	invoice_code =  rows[0].invoice_code;
                    else
                    	invoice_code = false;

                    cb( invoice_code );
                   
                }
                else {
                	console.log(err);
                	return false;
                }
            });
        }        
exports.insertNewPackName = function ( packname, cb) {	
        var post  = {  
        	name: packname, 
        	status: 'W'
        };   
        db.get().query('insert into pack_adresses SET ? ', post,  function(err, result) {
                    cb( err, result );
            });
        } 

exports.insertNewAddress = function ( addr, pack_id, cb) {	
		var post  = {  
			address: addr, 
			invoice_code: '',
			pack_address_id: pack_id 
		};

		db.get().query('insert into btc_adresses SET ? ', post, function(err, result) {
			cb( err, result );
			
		});
        }         
