BitPayXp
 Payment System based on Bitcoind.

---------------------
Installation:
---------------------

"npm install" in src folder 

---------------------
Ho to configure?
---------------------

Change Configuration in config.js:

1. server address
2. Shop URL address for sending information about transaction realtime
3. Mysql password
4. module.exports.DEBUGMODE = true/false  - security OFF(for testing and check), or ON

**************

Change config in bitcoin.conf. Put correct link to interaction beetween bitcoind and system :


walletnotify=curl http://localhost:8081/transactions/%s

blocknotify=curl http://localhost:8081/block/%s

restore database structure: 
mysql -u username -p
CREATE DATABASE paybtc;
exit
mysql -u root -p paybtc < paybtc_structure.sql

---------------------
How run?
---------------------

1. run mysql: sudo mysqld start
2. run system: "nodemon app.js" in folder
3. run bitcoind: "bitcoind -daemon "

---------------------
How check:
---------------------

http://localhost:8081/admin/111 - admin panel.

http://localhost:8081/shop_cart/33 - imitation shop requests.

---------------------
How to use?
---------------------

1. Show buyer BTC address assigned to invoice:

http://localhost:8081/invoice/:ID/:SECRET

:ID - invoice ID
:SECRET - secret code  = sha512(:ID, :SALT (from secure.js));

example:
http://localhost:8081/invoice/8/4226535f57daaecbbafb448b3f6c5c1abeb63e50e6edea938b6dd93aa4ad658c73286b6c918bc1490237fb36e309129bbf8b08e28bc8591ac5760671df630128


2. Get  transactions list of 100 from transaction N = :txfrom . 
http://localhost:8081/payments_from/:txfrom . /:secure


3. When new transaction come in real time we send information to SHOP on URL: SHOP_URL (configured in config.js)

http://SHOP_URL:SECRET

with post data:

	transaction_id
	invoice_code
	amount
	date

:SECRET - secret code  = sha512(:transaction_id, :SALT (from secure.js));



