
// server address, -/config to local for easy debug
adr = function(){
    switch(process.env.NODE_ENV){
        case 'server':
        console.log('server');
            return "211.110.229.64";

        case 'local':
        console.log('local');
            return "127.0.0.1";

        default:
        	console.log('server');
            return "211.110.229.64";
    }
};
adr_ip = function(){
    switch(process.env.NODE_ENV){
        case 'server':
        console.log('server');
            return "211.110.229.64";

        case 'local':
        console.log('local');
            return "127.0.0.1";

        default:
        	console.log('server');
            return "211.110.229.64";
    }
};
// Shop URL address for sending information about transaction realtime, -/config to local for easy debug
shop_url_reply = function(){
    switch(process.env.NODE_ENV){
        case 'server':
        console.log('server');
            return "211.110.229.64:8081/sendnewtx/";

        case 'local':
        console.log('local');
            return "127.0.0.1:8081/sendnewtx/";

        default:
        	console.log('server');
            return "211.110.229.64:8081/sendnewtx/";
    }
};
// Mysql password -/config to local for easy debug
mysqlpw = function(){
    switch(process.env.NODE_ENV){
        case 'server':
        console.log('server');
            return "remember0416";

        case 'local':
        console.log('local');
            return "root";

        default:
        	console.log('server');
            return "remember0416";
    }
};

module.exports.PORT = 8081;

//Passphrase for crypt decrypt wallet. Must remove when production.
module.exports.passphrase = "qqq";
module.exports.ADDRIP = adr_ip();
module.exports.ADDR = adr();
module.exports.MYSQLPW = mysqlpw();
module.exports.SHOP_URL = shop_url_reply();


// security OFF(for testing and check), or ON
module.exports.DEBUGMODE = true;

//start local: NODE_ENV=local node app.js OR: ./localstart
//start server: nodemon app.js


